import { DataGridModule } from './../../../demo-ng-kolekto/src/lib/data-grid/data-grid.module';
import { FormModule } from './../../../demo-ng-kolekto/src/lib/form/form.module';
import { DemoNgKolektoModule } from './../../../demo-ng-kolekto/src/lib/demo-ng-kolekto.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DemoNgKolektoModule,
    HttpClientModule,
    FormModule,
    //DataGridModule.forRoot({ bgColor: '#fbfbfb' })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
