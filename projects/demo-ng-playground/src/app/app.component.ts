import { FormValue, FieldConfig } from './../../../demo-ng-kolekto/src/lib/form/form.models';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Api } from "./utils/api";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  dGridData$!: Observable<{}[]>;
  formConfig$!: Observable<FieldConfig[]>;
  errorsForm1: any;
  errorsForm2: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.formConfig$ = this.http.get(Api.FORM_CONFIG_END_POINT).pipe(map((resp: any) => resp.data));
    this.dGridData$ = this.http.get(Api.ITEMS_END_POINT).pipe(map((resp: any) => resp.data));
  }

  more(ev: any) {
    alert(JSON.stringify(ev));
  }

  onSubmit(ev: FormValue) {
    this.errorsForm1 = ev.errors;
    console.log(ev.payload);
  }

}
