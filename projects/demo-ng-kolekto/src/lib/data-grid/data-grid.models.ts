export interface ItemModel {
    id?: number;
    category: string;
    imgSrc: string;
    price: number;
    title: string;
}
