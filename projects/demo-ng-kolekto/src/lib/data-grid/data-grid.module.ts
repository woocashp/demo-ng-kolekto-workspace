import { DataGridConfigService } from './data-grid.config.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { DataGridComponent } from './components/data-grid/data-grid.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [DataGridComponent],
  imports: [
    CommonModule
  ],
  exports: [DataGridComponent]
})
export class DataGridModule {
  static forRoot(config: DataGridConfigService): ModuleWithProviders<DataGridModule> {
    return {
      ngModule: DataGridModule,
      providers: [
        { provide: DataGridConfigService, useValue: config }
      ]
    }
  }
}
