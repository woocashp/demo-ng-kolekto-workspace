import { Component, Input, ContentChild, TemplateRef, ChangeDetectionStrategy } from '@angular/core';
import { DataGridConfigService } from '../../data-grid.config.service';

@Component({
  selector: 'app-data-grid',
  template: `
    <div *ngIf="!data?.length">you should pass data any[]</div>
    <div *ngIf="!tpl">you should pass ng-template</div>
    <table *ngIf="data?.length" class="my-table" [ngStyle]="{background: configService.bgColor}">
      <thead>
          <tr>
              <th *ngFor="let head of headers">
                  {{head}}
              </th>
          </tr>
      </thead>
      <tbody>
          <tr *ngFor="let row of data | slice:currentPage * pageSize:currentPage * pageSize + pageSize; let idx=index">
              <ng-container *ngTemplateOutlet="tpl; context: {$implicit: row, nr: idx+1}"></ng-container>
          </tr>
      </tbody>
    </table>
  `,
  styleUrls: ['./data-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataGridComponent {

  @Input() data!: any[] | null;
  @Input() headers!: string[];
  @ContentChild(TemplateRef) tpl!: TemplateRef<any>;

  public pageSize = 5;
  public currentPage = 0;

  constructor(public configService: DataGridConfigService) { }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
  }

}
