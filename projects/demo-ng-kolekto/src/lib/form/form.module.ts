import { RadioGroupComponent } from './components/fields/radio/radio-group.component';
import { NgModule } from '@angular/core';
import { FormGeneratorComponent } from './components/form-generator/form-generator.component';
import { FieldGeneratorDirective } from './directives/field-generator.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FormInputComponent } from './components/fields/input/form-input.component';
import { FormSelectComponent } from './components/fields/select/form-select.component';
import { FormTextareaComponent } from './components/fields/textarea/form-textarea.component';
import { FormEditableComponent } from './components/fields/contenteditable/form-editable.component';
import { CustomContentEditableComponent } from './components/controls/contenteditable/contenteditable.component';
import { ErrorsComponent } from './components/errors/errors.component';
import { FormButtonComponent } from './components/fields/form-button/form-button.component';
import { PasswordComponent } from './components/fields/password/password.component';
import { RadioComponent } from './components/controls/radio/radio.component';

@NgModule({
  declarations: [
    FormGeneratorComponent,
    FormInputComponent,
    FormSelectComponent,
    FieldGeneratorDirective,
    CustomContentEditableComponent,
    FormEditableComponent,
    FormTextareaComponent,
    ErrorsComponent,
    FormButtonComponent,
    PasswordComponent,
    RadioComponent,
    RadioGroupComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    FormGeneratorComponent,
    FieldGeneratorDirective
  ]
})
export class FormModule { }
