import { Component, OnInit } from '@angular/core';
import { FieldConfig } from '../../../form.models';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'lib-form-button',
  templateUrl: './form-button.component.html'
})
export class FormButtonComponent implements OnInit {

  config!: FieldConfig;
  form!: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
