import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../form.models';

@Component({
  template: `
  <div [formGroup]="form">
    <label class='label'>{{config.label}}</label>
    <app-contenteditable class="{{config.cssClass}}" [data]='config.value' [formControlName]="config.name"></app-contenteditable>
    <app-errors *ngIf="form.get(config.name)?.errors as errors" [data]="{errors: errors, validators: config.validators}">
  </app-errors>
  </div>
  `
})
export class FormEditableComponent {

  config!: FieldConfig;
  form!: FormGroup;

  constructor() { }

}
