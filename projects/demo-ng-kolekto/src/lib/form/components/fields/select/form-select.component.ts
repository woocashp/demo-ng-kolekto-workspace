import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../form.models';

@Component({
  templateUrl: './form-select.component.html'
})
export class FormSelectComponent {

  config!: FieldConfig;
  form!: FormGroup;

  constructor() { }

}
