import { FieldConfig } from '../../../form.models';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  template: `
    <div [formGroup]="form">
        <div class='label'>{{config.label}}</div>
        <app-radio [config]='config' [formControlName]="config.name"></app-radio>
        <app-errors *ngIf="form.get(config.name)?.errors as errors" [data]="{errors: errors, validators: config.validators}">
        </app-errors>
    </div>
    <br>
    `
})

export class RadioGroupComponent implements OnInit {

  config!: FieldConfig;
  form!: FormGroup;

  ngOnInit() { }
}
