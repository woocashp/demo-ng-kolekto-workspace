import { Component, OnInit } from '@angular/core';
import { FieldConfig } from '../../../form.models';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'lib-password',
  templateUrl: './password.component.html'
})
export class PasswordComponent implements OnInit {

  config!: FieldConfig;
  form!: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
