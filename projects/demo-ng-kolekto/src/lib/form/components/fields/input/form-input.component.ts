import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../form.models';

@Component({
  templateUrl: './form-input.component.html'
})
export class FormInputComponent {

  config!: FieldConfig;
  form!: FormGroup;

  constructor() { }

}
