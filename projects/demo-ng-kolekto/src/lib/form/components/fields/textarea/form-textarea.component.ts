import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../../form.models';

@Component({
  templateUrl: './form-textarea.component.html'
})
export class FormTextareaComponent implements OnInit {

  form!: FormGroup;
  config!: FieldConfig;

  constructor() { }

  ngOnInit() { }

  setTextareateHeight(value: string) {
    return value && value.split(/[\n\r]/g).length;
  }

}
