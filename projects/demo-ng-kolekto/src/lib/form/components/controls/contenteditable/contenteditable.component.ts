import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-contenteditable',
  styleUrls: ['./contenteditable.component.scss'],
  templateUrl: './contenteditable.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomContentEditableComponent),
      multi: true,
    }
  ]
})
export class CustomContentEditableComponent implements ControlValueAccessor {
  value: any;
  onTouch!: Function;
  onModelChange!: Function;
  @Input() data: any;

  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    throw new Error("Method not implemented.");
  }
  change(value: string) {
    this.onModelChange(value);
    this.writeValue(value);
  }
}
