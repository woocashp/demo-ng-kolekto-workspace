import { FORM_BG_COLOR } from './../../form.token';
import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ContentChild,
  TemplateRef,
  Inject,
  Optional,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { FieldConfig, FieldValidator, FormEvents, FormValue } from '../../form.models';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-form-generator',
  templateUrl: './form-generator.component.html',
  styles: [
    `
      ::ng-deep form > div {
        padding: 20px;
      }
      ::ng-deep .label {
        font-weight: bold;
      }
    `,
  ],
  exportAs: 'app-form-generator',
})
export class FormGeneratorComponent implements OnChanges {
  @Input() formConfig!: FieldConfig[] | null;
  @Output() action: EventEmitter<FormValue> = new EventEmitter();
  form: FormGroup;
  errors!: any[];
  @ContentChild(TemplateRef) tpl!: TemplateRef<any>;

  constructor(
    private fb: FormBuilder,
    @Optional() @Inject(FORM_BG_COLOR) public bgColor: string
  ) {
    this.form = this.fb.group({});
  }

  onSubmit() {
    this.errors = Object.entries(this.form.controls)
      .filter(([, { errors }]) => errors)
      .map(([name, { errors }]) => ({ [name]: errors }));

    this.action.next({
      type: FormEvents.submit,
      payload: this.form.value,
      errors: this.errors.length ? this.errors : null,
    });
  }

  createForm() {
    this.formConfig
      ?.filter(({ type }) => type !== 'button')
      .forEach(({ name, value, validators, id }) => {
        if (this.form.controls[name]) {
          this.form.setControl(
            name,
            this.fb.control(value, this.getValidators(validators))
          );
        } else {
          this.form.addControl(
            name,
            this.fb.control(value, this.getValidators(validators))
          );
        }
        this.form.controls[name].valueChanges
          .pipe(debounceTime(1000))
          .subscribe((value) => {
            this.action.next({ type: FormEvents.update, payload: { [name]: value } });
          });
      });
  }

  getValidators(validators: FieldValidator[] | undefined): ValidatorFn[] | null {
    if (!validators) return null;
    return validators.map((validator: FieldValidator): ValidatorFn => {
      return validator.name in Validators
        ? validator.param
          ? (Validators[validator.name] as Function)(validator.param)
          : Validators[validator.name]
        : null;
    });
  }

  ngOnChanges({ formConfig: { currentValue } }: SimpleChanges): void {
    currentValue && this.createForm();
  }
}
