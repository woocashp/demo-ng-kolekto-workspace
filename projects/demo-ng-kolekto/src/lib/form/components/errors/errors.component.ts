import { Component, Input } from '@angular/core';
import { FieldValidator } from '../../form.models';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})
export class ErrorsComponent {

  @Input() data!: { errors: ValidationErrors, validators: FieldValidator[] | undefined };

  getMessage(key: string) {
    try {
      return this.data.validators?.find((validator) => key === validator.name.toLowerCase())?.message;
    } catch (error) {
      console.log(`missing validator ${key}`);
    }
  }

}
