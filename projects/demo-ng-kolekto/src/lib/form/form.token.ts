import { InjectionToken } from '@angular/core';

export const FORM_BG_COLOR = new InjectionToken<string>('',{
  factory(){
    return 'whitesmoke'
  }
});
