import { RadioGroupComponent } from '../components/fields/radio/radio-group.component';
import { Directive, ViewContainerRef, ComponentFactoryResolver, OnInit, Input, ComponentRef } from '@angular/core';
import { FieldConfig, FieldTypes } from '../form.models';
import { FormGroup } from '@angular/forms';
import { FormInputComponent } from '../components/fields/input/form-input.component';
import { FormSelectComponent } from '../components/fields/select/form-select.component';
import { FormTextareaComponent } from '../components/fields/textarea/form-textarea.component';
import { FormEditableComponent } from '../components/fields/contenteditable/form-editable.component';
import { FormButtonComponent } from '../components/fields/form-button/form-button.component';
import { PasswordComponent } from '../components/fields/password/password.component';

type Components = FormInputComponent | FormSelectComponent | FormTextareaComponent | FormEditableComponent;

const fields: { [key in FieldTypes]: any } = {
  input: FormInputComponent,
  password: PasswordComponent,
  textarea: FormTextareaComponent,
  contenteditable: FormEditableComponent,
  select: FormSelectComponent,
  button: FormButtonComponent,
  radio: RadioGroupComponent
}

@Directive({
  selector: '[appFieldGenerator]'
})
export class FieldGeneratorDirective implements OnInit {

  @Input() appFieldGenerator!: { config: FieldConfig, form: FormGroup };

  constructor(
    private container: ViewContainerRef
  ) { }

  ngOnInit(): void {
    const fieldType = this.appFieldGenerator.config.type;
    try {
      const component: ComponentRef<unknown> = this.container.createComponent(fields[fieldType]);
      const componentInstance = <Components>component.instance;
      componentInstance.config = this.appFieldGenerator.config;
      componentInstance.form = this.appFieldGenerator.form;
    } catch (error) {
      console.warn(`component ${fieldType} not exists`);
    }
  }

}
