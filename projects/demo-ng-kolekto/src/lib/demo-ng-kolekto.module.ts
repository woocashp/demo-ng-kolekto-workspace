import { FormModule } from './form/form.module';
import { DataGridModule } from './data-grid/data-grid.module';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [FormsModule, CommonModule],
  exports: [DataGridModule, FormModule]
})
export class DemoNgKolektoModule { }
