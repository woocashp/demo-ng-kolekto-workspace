# eg. x-publish xx true dir

if [ "$1" ]; then
    cd projects/demo-ng-kolekto
    npm version minor
    cd ../..
    ng build demo-ng-kolekto --configuration production
    npm publish dist/demo-ng-kolekto
    git add .
    #git tag v$2
    git commit -m "$1"
    npm version minor
    git push origin master
    #git push --tags origin
else
    echo 'commit name missing!'
fi
