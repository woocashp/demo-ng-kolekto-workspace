# DemoNgModules

Angular Libraries
From Components to Angular Package Format

### install
`npm i demo-ng-kolekto`

### to see demo run
`npm start`

## Usage in your app

### import
`import { DataGridModule, FormModule } from 'demo-ng-kolekto';`
add this modules to yours angular module import section

### usage data-grid
`<app-data-grid [data]="{}[]" [headers]="string[]">
    <ng-template let-item>
    <td>{{item.name}}</td>
    <td>{{item.phone}}</td>
    </ng-template>
</app-data-grid>`

### usage form-generator
`<app-form-generator (action)="onSubmit($event)" [formConfig]="FieldConfig[]"></app-form-generator>`
